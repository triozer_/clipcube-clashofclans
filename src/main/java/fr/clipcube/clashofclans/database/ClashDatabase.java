package fr.clipcube.clashofclans.database;

import fr.clipcube.clashofclans.player.GamePlayer;

import java.sql.*;
import java.util.UUID;

/**
 * @author Triozer
 */
public class ClashDatabase {
    private String host;
    private String username;
    private String password;
    private String database;

    private int port;

    private Connection connection;

    public ClashDatabase(String host, int port, String database, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.database = database;

        this.port = port;
    }

    public void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean hasAccount(GamePlayer gamePlayer) {
        connect();

        ResultSet resultSet = query("SELECT * FROM `coc` WHERE `uuid`='" + gamePlayer.getClipPlayer().getUniqueId() + "';");

        try {
            if (resultSet.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return false;
    }

    public void init(GamePlayer gamePlayer) {
        if (!hasAccount(gamePlayer)) {
            connect();
            try {
                Statement statement = connection.createStatement();

                execute("INSERT INTO `coc` (`id`, `uuid`, `elixir`, `gems`, `gold`) " +
                        "VALUES (NULL, '" + gamePlayer.getClipPlayer().getUniqueId() + "', '0', '1000', '1000');");

                statement.close();
            } catch (SQLException e) {
                if (e.getMessage().contains("Duplicate entry '" + gamePlayer.getClipPlayer().getUniqueId() + "' for key 'uuid'"))
                    return;

                e.printStackTrace();
            }

            disconnect();
        }

        cache(gamePlayer);
    }

    private void cache(GamePlayer gamePlayer) {
        int elixir = getElixir(gamePlayer.getClipPlayer().getUniqueId());
        int gems   = getGems(gamePlayer.getClipPlayer().getUniqueId());
        int gold   = getGold(gamePlayer.getClipPlayer().getUniqueId());

        gamePlayer.getCache().setElixir(elixir);
        gamePlayer.getCache().setGems(gems);
        gamePlayer.getCache().setGold(gold);
    }

    public int getElixir(UUID uuid) {
        connect();

        int elixir = 0;

        ResultSet resultSet = query("SELECT `elixir` FROM `coc` WHERE `uuid`='" + uuid + "';");

        try {
            while (resultSet.next()) {
                elixir = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return elixir;
    }

    public int getGems(UUID uuid) {
        connect();

        int gems = 0;

        ResultSet resultSet = query("SELECT `gems` FROM `coc` WHERE `uuid`='" + uuid + "';");

        try {
            while (resultSet.next()) {
                gems = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return gems;
    }

    public int getGold(UUID uuid) {
        connect();

        int gold = 0;

        ResultSet resultSet = query("SELECT `gold` FROM `coc` WHERE `uuid`='" + uuid + "';");

        try {
            while (resultSet.next()) {
                gold = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return gold;
    }

    public void execute(String query) {
        connect();

        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

            statement.close();
        } catch (Exception ex) {
            if (!ex.getMessage().contains("Duplicate entry"))
                System.err.println(ex);

            disconnect();
        }

        disconnect();
    }

    public ResultSet query(String query) {
        ResultSet resultSet = null;

        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (Exception ex) {
            System.err.println(ex);
        }

        return resultSet;
    }
}
