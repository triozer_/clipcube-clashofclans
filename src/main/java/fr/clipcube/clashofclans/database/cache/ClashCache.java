package fr.clipcube.clashofclans.database.cache;

import fr.clipcube.clashofclans.ClashOfClans;
import fr.clipcube.clashofclans.player.GamePlayer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Triozer
 */
public class ClashCache {

    private Map<String, Object> cache = new HashMap<>();
    private GamePlayer gamePlayer;

    public ClashCache(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;

        cache.put("elixir", 0);
        cache.put("gems", 1000);
        cache.put("gold", 1000);
    }

    public void save() {
        ClashOfClans.getClashDatabase().execute("UPDATE `coc` SET elixir='" + getElixir() + "', gems='" + getGems() + "',"
                + " gold='" + getGold() + "' WHERE uuid='" + gamePlayer.getClipPlayer().getUniqueId() + "';");
    }

    public int getGold() {
        return (int) cache.get("gold");
    }

    public void addGold(int gold) {
        cache.replace("gold", getGold() + gold);

        gamePlayer.updateScoreboard();
    }

    public void removeGold(int gold) {
        if ((int) cache.get("gold") - gold >= 0)
            cache.replace("gold", getGold() - gold);

        gamePlayer.updateScoreboard();
    }

    public void setGold(int gold) {
        cache.replace("gold", gold);

        if (gamePlayer.getClipPlayer().getIndividualScoreboard() != null)
            gamePlayer.updateScoreboard();
    }

    public int getGems() {
        return (int) cache.get("gems");
    }

    public void addGems(int gems) {
        cache.replace("gems", getGold() + gems);

        gamePlayer.updateScoreboard();
    }

    public void removeGems(int gems) {
        if ((int) cache.get("gems") - gems >= 0)
            cache.replace("gems", getGold() - gems);

        if (gamePlayer.getClipPlayer().getIndividualScoreboard() != null)
            gamePlayer.updateScoreboard();
    }

    public void setGems(int gems) {
        cache.replace("gems", gems);

        if (gamePlayer.getClipPlayer().getIndividualScoreboard() != null)
            gamePlayer.updateScoreboard();
    }

    public int getElixir() {
        return (int) cache.get("elixir");
    }

    public void addElixir(int elixir) {
        cache.replace("elixir", getGold() + elixir);

        gamePlayer.updateScoreboard();
    }

    public void removeElixir(int elixir) {
        if ((int) cache.get("elixir") - elixir >= 0)
            cache.replace("elixir", getGold() - elixir);

        gamePlayer.updateScoreboard();
    }

    public void setElixir(int elixir) {
        cache.replace("elixir", elixir);

        if (gamePlayer.getClipPlayer().getIndividualScoreboard() != null)
            gamePlayer.updateScoreboard();
    }

}
