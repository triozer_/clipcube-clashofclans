package fr.clipcube.clashofclans;

import fr.clipcube.api.spigot.game.gamemode.IClipGamemode;
import fr.clipcube.clashofclans.command.BuildCommand;
import fr.clipcube.clashofclans.command.VillageCommand;
import fr.clipcube.clashofclans.database.ClashDatabase;
import fr.clipcube.clashofclans.listener.DefaultListener;
import fr.clipcube.clashofclans.player.GamePlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Triozer
 */
public class ClashOfClans extends JavaPlugin implements IClipGamemode {

    private static ClashOfClans      instance;
    private static GamePlayerManager gamePlayerManager;
    private static ClashDatabase     clashDatabase;

    public static ClashOfClans getInstance() {
        return instance;
    }

    public static GamePlayerManager getGamePlayerManager() {
        return gamePlayerManager;
    }

    public static ClashDatabase getClashDatabase() {
        return clashDatabase;
    }

    @Override
    public void onEnable() {
        instance = this;

        System.out.println(" ");
        System.out.println(getGamePrefix() + ": " + getGameDescription() + " by " + getAuthor());
        System.out.println(" ");

        gamePlayerManager = new GamePlayerManager();
        clashDatabase = new ClashDatabase("127.0.0.1", 3307, "clip", "root", "");

        registerCommands();
        registerListeners();
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new DefaultListener(), this);
    }

    private void registerCommands() {
        new BuildCommand().register(this);
        new VillageCommand().register(this);
    }

    public String getGameName() {
        return "ClashOfClans";
    }

    public String getGameDescription() {
        return "ClashOfClans on Minecraft !";
    }

    public String getAuthor() {
        return "Triozer";
    }

    public String getGamePrefix() {
        return "§l[" + getGameName() + "] §r";
    }
}
