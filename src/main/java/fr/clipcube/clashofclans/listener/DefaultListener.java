package fr.clipcube.clashofclans.listener;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.scoreboard.IndividualScoreboard;
import fr.clipcube.api.spigot.util.Text;
import fr.clipcube.clashofclans.ClashOfClans;
import fr.clipcube.clashofclans.player.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Triozer
 */
public class DefaultListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());
        clipPlayer.teleport(Bukkit.getWorld("world").getSpawnLocation());
        clipPlayer.sendCenteredMessage("Tu es connecté sur le mode de jeu : §b§l" + ClashOfClans.getInstance().getGameName() + "§r.");
        clipPlayer.sendVoid();
        clipPlayer.sendMessage(ClashOfClans.getInstance().getGamePrefix() + "§7Utilise la commande §6/village §7pour te téléporter à ton village.");
        clipPlayer.sendVoid();

        GamePlayer gamePlayer = ClashOfClans.getGamePlayerManager().getPlayer(clipPlayer);

        clipPlayer.make(new IndividualScoreboard(clipPlayer.getPlayer(), ClashOfClans.getInstance().getGameName()));

        clipPlayer.getIndividualScoreboard().setLine(0, "§1");
        clipPlayer.getIndividualScoreboard().setLine(1, "§f§lElixir: §d§l" + gamePlayer.getElixir() + "/" + gamePlayer.getElixirStorage());
        clipPlayer.getIndividualScoreboard().setLine(2, "§f§lGemmes: §a§l" + gamePlayer.getGems() + "/" + gamePlayer.getGemsStorage());
        clipPlayer.getIndividualScoreboard().setLine(3, "§f§lGold: §6§l" + gamePlayer.getGold() + "/" + gamePlayer.getGoldStorage());
        clipPlayer.getIndividualScoreboard().setLine(4, "§4");

        clipPlayer.sendTablist(Text.TABLIST_HEADER + "\n\n" + ClashOfClans.getInstance().getGameName() + "\n", "\n" + Text.TABLIST_FOOTER);

        event.setJoinMessage(null);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
    }

}
