package fr.clipcube.clashofclans.schematic;

import org.jnbt.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SchematicUtils {
    private static File baseSchematicsFile;

    private static List<Schematic> allSchematics = new ArrayList<Schematic>();

    public static void initFile(File baseFile) {
        baseSchematicsFile = baseFile;
    }

    public static void initSchematics() {
        allSchematics.clear();

        for (File schematicFile : baseSchematicsFile.listFiles()) {
            if (!(schematicFile.getName().startsWith("."))) {
                Schematic schematic = loadScheamtic(schematicFile);

                if (schematic != null) {
                    allSchematics.add(schematic);
                }
            }
        }
    }

    public static List<Schematic> getAllSchematics() {
        return allSchematics;
    }

    public static Schematic loadScheamtic(File file) {
        try {
            if (file.exists()) {
                NBTInputStream   nbtStream = new NBTInputStream(new FileInputStream(file));
                CompoundTag      compound  = (CompoundTag) nbtStream.readTag();
                Map<String, Tag> tags      = compound.getValue();
                Short            width     = ((ShortTag) tags.get("Width")).getValue();
                Short            height    = ((ShortTag) tags.get("Height")).getValue();
                Short            length    = ((ShortTag) tags.get("Length")).getValue();

                String materials = ((StringTag) tags.get("Materials")).getValue();

                byte[]  data    = ((ByteArrayTag) tags.get("Data")).getValue();
                byte[]  blockId = ((ByteArrayTag) tags.get("Blocks")).getValue();
                byte[]  addId   = new byte[0];
                short[] blocks  = new short[blockId.length];

                // We support 4096 block IDs using the same method as vanilla Minecraft, where
                // the highest 4 bits are stored in a separate byte array.
                if (tags.containsKey("AddBlocks")) {
                    addId = ((ByteArrayTag) tags.get("AddBlocks")).getValue();
                }

                // Combine the AddBlocks data with the first 8-bit block ID
                for (int index = 0; index < blockId.length; index++) {
                    if ((index >> 1) >= addId.length) { // No corresponding AddBlocks index
                        blocks[index] = (short) (blockId[index] & 0xFF);
                    } else {
                        if ((index & 1) == 0) {
                            blocks[index] = (short) (((addId[index >> 1] & 0x0F) << 8) + (blockId[index] & 0xFF));
                        } else {
                            blocks[index] = (short) (((addId[index >> 1] & 0xF0) << 4) + (blockId[index] & 0xFF));
                        }
                    }
                }

                nbtStream.close();

                Schematic schematic = new Schematic(file.getName().replace(".schematic", ""), width, height, length, materials, blocks, data);

                return schematic;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}