package fr.clipcube.clashofclans.schematic;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.util.HologramBuilder;
import fr.clipcube.clashofclans.ClashOfClans;
import fr.clipcube.clashofclans.player.GamePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class Building {
    private GamePlayer gamePlayer;

    private boolean finished = false;
    private boolean allowed;

    private String    name;
    private String    buildName;
    private Schematic schematic;
    private List<Block> allBlocks    = new ArrayList<>();
    private List<Block> placedBlocks = new ArrayList<>();

    private long   startingTime;
    private double estimatedTime;
    private long   finishing;
    private double actual;

    private BukkitTask      bukkitTask;
    private HologramBuilder hologramBuilder;

    private double price;
    private int    level;

    public Building(GamePlayer gamePlayer, Schematic schematic, int price) {
        this.gamePlayer = gamePlayer;

        if (gamePlayer.getGold() >= price) {
            allowed = true;

            this.schematic = schematic;
            this.name = schematic.getName();

            gamePlayer.getCache().removeGold(price);
        } else {
            allowed = false;

            gamePlayer.getClipPlayer().sendMessage("§cPas assez d'or.");
        }
    }

    public String getName() {
        return this.name;
    }

    public Schematic getSchematic() {
        return this.schematic;
    }

    public void build(final Location loc) {
        final HashMap<Block, Integer> blocks = new HashMap<Block, Integer>();

        startingTime = System.currentTimeMillis();

        for (int x = 0; x < schematic.getWidth(); x++) {
            for (int y = 0; y < schematic.getHeight(); y++) {
                for (int z = 0; z < schematic.getLength(); ++z) {
                    Location temp  = loc.clone().add(x, y, z);
                    Block    block = temp.getBlock();
                    int      index = y * schematic.getWidth() * schematic.getLength() + z * schematic.getWidth() + x;

                    Material material = Material.getMaterial(schematic.getBlocks()[index]);

                    if (material != Material.AIR) {
                        block.getDrops().clear();

                        blocks.put(block, index);
                        allBlocks.add(block);
                    }
                }
            }
        }

        final List<Block> orderedBlocks = new ArrayList<Block>();

        orderedBlocks.addAll(allBlocks);

        Collections.sort(orderedBlocks, new Comparator<Block>() {
            @Override
            public int compare(Block block1, Block block2) {
                return Double.compare(block1.getY(), block2.getY());
            }
        });

        final int  size          = orderedBlocks.size();
        final int  blocksPerTime = 1;
        final long delay         = 1L;
        hologramBuilder = new HologramBuilder(orderedBlocks.get(0).getLocation().add(0, 1, 0))
                .lines("§bBUILDER", "§7" + getName(), "", "", "§aSTARTING").build();

        estimatedTime = size * 0.05;
        estimatedTime = Math.floor(estimatedTime * 100) / 100;

        if (size > 0) {
            bukkitTask = new BukkitRunnable() {
                int index = 0;
                long time = startingTime;

                @Override
                public void run() {
                    for (int i = 0; i < blocksPerTime; i++) {
                        if (index < size) {
                            Block block      = orderedBlocks.get(index);
                            int   otherIndex = blocks.get(block);
                            int   typeId     = schematic.getBlocks()[otherIndex];
                            byte  data       = schematic.getData()[otherIndex];

                            Material material = Material.getMaterial(typeId);

                            if (!(block.getLocation().equals(loc)))
                                regenerateBlock(block, material, data);

                            if (material != Material.AIR)
                                placedBlocks.add(block);

                            index += 1;
                            time += 50;
                            finishing = time;
                            actual = Math.floor((estimatedTime - (time - startingTime) / 1000.0) * 100) / 100;

                            hologramBuilder
                                    .updateLines("§bBUILDER", "§7" + getName(), "", "§a" + actual + "s", "§a" + 100 * index / orderedBlocks.size() + "%");
                        } else {
                            if (ClipAPI.getPlayerManager().getPlayers().contains(gamePlayer.getClipPlayer()))
                                gamePlayer.getClipPlayer().sendMessage("La construction " + getName() + " vient de se terminer.");
                            else
                                gamePlayer.addFinished(Building.this);

                            hologramBuilder.end();
                            finished = true;

                            this.cancel();
                            return;
                        }
                    }
                }
            }.runTaskTimer(ClashOfClans.getInstance(), 40L, delay);
        }
    }

    public void destroy() {
        final List<Block> orderedBlocks = new ArrayList<Block>();

        orderedBlocks.addAll(placedBlocks);

        Collections.sort(orderedBlocks, new Comparator<Block>() {
            @Override
            public int compare(Block block1, Block block2) {
                return Double.compare(block1.getY(), block2.getY());
            }
        });

        final int  size          = orderedBlocks.size();
        final int  blocksPerTime = 1;
        final long delay         = 1L;
        hologramBuilder = new HologramBuilder(orderedBlocks.get(0).getLocation().add(0, 1, 0))
                .lines("UNBUILDER", getName(), "", "STARTING").build();

        if (size > 0) {
            new BukkitRunnable() {
                int index = 0;

                @Override
                public void run() {
                    for (int i = 0; i < blocksPerTime; i++) {
                        if (index < size) {
                            Block block = orderedBlocks.get(index);

                            block.setType(Material.AIR);

                            index += 1;

                            hologramBuilder.updateLines(100 * index / orderedBlocks.size() + "%", "", "", "");
                            hologramBuilder.getArmorStands().forEach(armorStand -> armorStand.teleport(block.getLocation().add(0, 1, 0)));
                        } else {
                            hologramBuilder.end();

                            this.cancel();
                            return;
                        }
                    }
                }
            }.runTaskTimer(ClashOfClans.getInstance(), 40L, delay);
        }

        gamePlayer.remove(this);
    }

    public void stop() {
        gamePlayer.getClipPlayer().sendMessage("La construction " + getName() + " vient de se terminer.");
        hologramBuilder.end();
        finished = true;

        bukkitTask.cancel();
    }

    public void upgrade() {

    }

    private int getLevel() {
        return level;
    }

    public double getPrice() {
        return price;
    }

    public double getTime() {
        return finishing;
    }

    public double getActualTime() {
        return actual;
    }

    public long getStartingTime() {
        return startingTime;
    }

    public long getFinishing() {
        return finishing;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void regenerateBlock(Block block, final Material type, final byte data) {
        if (type == null || type == Material.AIR)
            return;

        block.setTypeIdAndData(type.getId(), data, false);
    }
}