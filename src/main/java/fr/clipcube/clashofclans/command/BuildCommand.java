package fr.clipcube.clashofclans.command;

import fr.clipcube.api.spigot.command.AbstractCommand;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.clashofclans.ClashOfClans;
import fr.clipcube.clashofclans.player.GamePlayer;
import fr.clipcube.clashofclans.schematic.Building;
import fr.clipcube.clashofclans.schematic.SchematicUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Triozer
 */
public class BuildCommand extends AbstractCommand {
    public BuildCommand() {
        super("build", "coc.build", true, 1);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        GamePlayer player = ClashOfClans.getGamePlayerManager().getPlayer(sender);

        if (args[0].equalsIgnoreCase("remove")) {
            player.getBuildingList().forEach(building -> {
                building.destroy();
                try {
                    player.getBuildingList().remove(building);
                } catch (ConcurrentModificationException e) {
                    return;
                }
            });
        } else if (args[0].equalsIgnoreCase("destroy")) {
            sender.sendVoid();
            sender.sendMessage("Destruction de: " + args[1]);

            Building building = player.getBuilding(args[1]);
            if (!building.isFinished()) {
                building.stop();
                building.destroy();
            } else {
                building.destroy();
            }

            player.getBuildingList().remove(building);

            sender.sendVoid();
        } else if (args[0].equalsIgnoreCase("list")) {
            sender.sendVoid();

            if (player.getBuildingList().size() > 0)
                player.getBuildingList().forEach(building -> {
                    List<String> notFinished = new ArrayList<>();
                    List<String> finished    = new ArrayList<>();

                    if (!building.isFinished()) {
                        notFinished.add("§7[EN COURS] §6" + building.getName() + ": §7Commencée le " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(building.getStartingTime())) + " et se termine dans : " + building.getActualTime() + " seconde(s).");
                    } else {
                        finished.add("§7[TERMINÉ] §6" + building.getName());
                    }

                    sender.sendMessage(notFinished);
                    sender.sendMessage(finished);
                });
            else {
                sender.sendMessage("§cAucune construction.");
            }

            sender.sendVoid();

        } else {
            File file = new File(ClashOfClans.getInstance().getDataFolder(), "/schematics/" + args[0] + ".schematic");

            if (!file.exists()) {
                sender.sendVoid();
                sender.sendMessage("§cVeuillez spécifier un fichier valide. (" + args[0] + ".schematic n'existe pas)");
                sender.sendVoid();
            } else {
                Building building = new Building(player, SchematicUtils.loadScheamtic(file), 250);

                if (building.isAllowed())
                    player.build(building);
            }
        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendMessage("§cVeuillez spécifier un bâtiment.");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        List<String> list = new ArrayList<>();

        if (args[0].startsWith("r")) {
            list.add("remove");
        } else if (args[0].startsWith("l")) {
            list.add("list");
        } else if (args[0].startsWith("d")) {
            if (args.length > 1) {
                list.addAll(ClashOfClans.getGamePlayerManager().getPlayer(sender).getBuildingList().stream()
                        .map(Building::getName).collect(Collectors.toList()));
            } else
                list.add("destroy");
        }

        for (File file : new File(ClashOfClans.getInstance().getDataFolder(), "/schematics").listFiles())
            if (file.getName().contains(".schematic"))
                if (args.length > 0) {
                    if (file.getName().startsWith(args[0]))
                        list.add(file.getName().replaceAll(".schematic", ""));
                } else
                    list.add(file.getName().replaceAll(".schematic", ""));

        return list;
    }
}
