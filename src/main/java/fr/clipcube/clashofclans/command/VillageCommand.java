package fr.clipcube.clashofclans.command;

import fr.clipcube.api.spigot.command.AbstractCommand;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.clashofclans.ClashOfClans;

import java.util.List;

/**
 * @author Triozer
 */
public class VillageCommand extends AbstractCommand {

    public VillageCommand() {
        super("village", null, true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        ClashOfClans.getGamePlayerManager().getPlayer(sender).create();
    }

    @Override
    public void displayHelp(ClipPlayer sender) {

    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
