package fr.clipcube.clashofclans.player;

import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.util.TextBuilder;
import fr.clipcube.clashofclans.database.cache.ClashCache;
import fr.clipcube.clashofclans.schematic.Building;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Triozer
 */
public class GamePlayer {

    private ClipPlayer clipPlayer;
    private ClashCache cache;

    private World village;
    private List<Building>        finishedBuildingList = new ArrayList<>();
    private List<Building>        buildingList         = new ArrayList<>();
    private Map<String, Building> buildings            = new HashMap<>();

    private boolean first = true;

    public GamePlayer(ClipPlayer clipPlayer) {
        this.clipPlayer = clipPlayer;

        cache = new ClashCache(this);
    }

    public void create() {
        Bukkit.getWorlds().stream().filter(world -> !world.getName().equals(clipPlayer.getName())).forEach(world -> first = false);

        village = WorldCreator.name(clipPlayer.getName()).type(WorldType.FLAT).generatorSettings("3;0;2")
                .generateStructures(false).createWorld();

        village.setSpawnLocation(0, 1, 0);

        clipPlayer.setGameMode(GameMode.CREATIVE);

        if (first) {
            clipPlayer.sendMessage("§cVEUILLEZ PATIENTER PENDANT LA CREATION DE VOTRE MONDE.");

            village.getSpawnLocation().subtract(0, 1, 0).getBlock().setType(Material.IRON_BLOCK);
            clipPlayer.teleport(village.getSpawnLocation());
        } else {
            clipPlayer.teleport(village.getSpawnLocation());

            clipPlayer.sendMessage("Dernières quêtes: ");
            // clipPlayer.sendTextComponent(new TextBuilder("").click(ClickEvent.Action.SUGGEST_COMMAND, "").build());
            clipPlayer.sendVoid();
            clipPlayer.sendMessage("Constructions en cours: ");

            getBuildingList().stream().filter(building1 -> !building1.isFinished()).forEach(building -> {
                clipPlayer.sendTextComponent(new TextComponent("§6" + building.getName() + ": "),
                        new TextBuilder("§a[TELEPORT]").click(ClickEvent.Action.RUN_COMMAND, ".teleport").build(), new TextComponent(" "),
                        new TextBuilder("§c[CANCEL]").click(ClickEvent.Action.RUN_COMMAND, "/build destroy " + building.getName())
                                .hove(HoverEvent.Action.SHOW_TEXT, "Votre bâtiment risque d'être détruit.").build());
            });

            if (!finishedBuildingList.isEmpty()) {
                clipPlayer.sendVoid();
                clipPlayer.sendMessage("Constructions terminées: ");

                getFinishedBuildingList().stream().filter(building1 -> !building1.isFinished()).forEach(building -> {
                    clipPlayer.sendTextComponent(new TextComponent("§6" + building.getName() + ": "),
                            new TextBuilder("§a[TELEPORT]").click(ClickEvent.Action.RUN_COMMAND, ".teleport").build());
                });
            }

            clipPlayer.sendVoid();
        }
    }

    public void addFinished(Building building) {
    }

    public void build(Building building) {
        building.build(clipPlayer.getLocation());

        buildingList.add(building);
        buildings.put(building.getName(), building);
    }

    public void remove(Building building) {
        buildingList.remove(building);
        buildings.remove(building.getName());
    }

    public void updateScoreboard() {
        clipPlayer.getIndividualScoreboard().setLine(0, "§1");
        clipPlayer.getIndividualScoreboard().setLine(1, "§f§lElixir: §d§l" + getElixir() + "/" + getElixirStorage());
        clipPlayer.getIndividualScoreboard().setLine(2, "§f§lGemmes: §a§l" + getGems() + "/" + getGemsStorage());
        clipPlayer.getIndividualScoreboard().setLine(3, "§f§lGold: §6§l" + getGold() + "/" + getGoldStorage());
        clipPlayer.getIndividualScoreboard().setLine(4, "§4");
    }

    public int getElixir() {
        return cache.getElixir();
    }

    public int getGems() {
        return cache.getGems();
    }

    public int getGold() {
        return cache.getGold();
    }

    public List<Building> getBuildingList() {
        return buildingList;
    }

    public List<Building> getFinishedBuildingList() {
        return finishedBuildingList;
    }

    public boolean isFirst() {
        return first;
    }

    public Building getBuilding(String key) {
        return buildings.get(key);
    }

    public World getVillage() {
        return village;
    }

    public ClipPlayer getClipPlayer() {
        return clipPlayer;
    }

    public ClashCache getCache() {
        return cache;
    }

    public int getElixirStorage() {
        return 1000;
    }

    public int getGemsStorage() {
        return 1000;
    }

    public int getGoldStorage() {
        return 1000;
    }
}