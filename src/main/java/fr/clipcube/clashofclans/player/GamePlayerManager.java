package fr.clipcube.clashofclans.player;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.clashofclans.ClashOfClans;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;

/**
 * @author Triozer
 */
public class GamePlayerManager {

    private List<GamePlayer>      players;
    private Map<UUID, GamePlayer> uuidToGamePlayer;

    public GamePlayerManager() {
        players = new ArrayList<>();
        uuidToGamePlayer = new HashMap<>();

        Bukkit.getPluginManager().registerEvents(new PlayerListeners(), ClashOfClans.getInstance());
    }

    public void addPlayer(GamePlayer gamePlayer) {
        players.add(gamePlayer);
        uuidToGamePlayer.put(gamePlayer.getClipPlayer().getUniqueId(), gamePlayer);

        ClashOfClans.getClashDatabase().init(gamePlayer);
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public GamePlayer getPlayer(UUID uuid) {
        return uuidToGamePlayer.get(uuid);
    }

    public GamePlayer getPlayer(ClipPlayer player) {
        return uuidToGamePlayer.get(player.getUniqueId());
    }

    public final class PlayerListeners implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPlayerLogin(PlayerLoginEvent event) {
            ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());

            if (!players.contains(getPlayer(clipPlayer)))
                addPlayer(new GamePlayer(clipPlayer));
        }

        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent event) {
            getPlayer(event.getPlayer().getUniqueId()).getCache().save();
        }
    }

}
